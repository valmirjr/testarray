package br.com.valmirsystems.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//@Data
//@AllArgsConstructor
//@NoArgsConstructor
public class Customer {

	private String idCustomer;
	private String customerName;
	
	public Customer() {
	}
	
	public Customer(String idCustomer, String customerName) {
		this.idCustomer = idCustomer;
		this.customerName = customerName;
	}


	public String getIdCustomer() {
		return idCustomer;
	}


	public void setIdCustomer(String idCustomer) {
		this.idCustomer = idCustomer;
	}


	public String getCustomerName() {
		return customerName;
	}


	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	@Override
	public String toString() {
		return "[idCustomer=" + idCustomer + ", customerName=" + customerName + "]";
	}
	
	
	
	
}
