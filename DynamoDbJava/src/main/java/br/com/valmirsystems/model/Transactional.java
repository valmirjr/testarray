package br.com.valmirsystems.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIndexHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverted;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBVersionAttribute;

import br.com.valmirsystems.translator.CustomerTranslator;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

//@Data
//@AllArgsConstructor
//@NoArgsConstructor
//@Getter
//@Setter
@DynamoDBTable(tableName = "Transactional")
public class Transactional {
	@DynamoDBHashKey(attributeName = "pk")
	private String pk;
//	@DynamoDBRangeKey(attributeName = "date")
//	@DynamoDBIndexHashKey(attributeName = "date", globalSecondaryIndexName = "date-index")
	@DynamoDBAttribute(attributeName = "date")
	private String date;
	@DynamoDBAttribute(attributeName = "amount")
	private Integer amount;
	@DynamoDBVersionAttribute(attributeName = "version")
	private Long version;
	@DynamoDBAttribute(attributeName = "customer")
	@DynamoDBTypeConverted(converter = CustomerTranslator.class)
	private Customer customer;
	
	public Transactional() {
	}
	
	public Transactional(String pk, String date, Integer amount, Customer customer) {
		this.pk = pk;
		this.date = date;
		this.amount = amount;
		this.customer = customer;
	}
	public String getPk() {
		return pk;
	}
	public void setPk(String pk) {
		this.pk = pk;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Override
	public String toString() {
		return "Transactional [pk=" + pk + ", date=" + date + ", amount=" + amount + ", version=" + version
				+ ", customer=" + customer + "]";
	}
	
	
	
}
