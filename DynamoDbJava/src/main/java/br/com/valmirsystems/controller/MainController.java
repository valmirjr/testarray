package br.com.valmirsystems.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.TableCollection;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ListTablesResult;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.TableDescription;

import br.com.valmirsystems.model.Customer;
import br.com.valmirsystems.model.Transactional;

@RestController
public class MainController {
	
	private DynamoDBMapper mapper;
	private DynamoDB dynamoDB;
	String tableName = "Transactional";
	
	public MainController() {
		AWSCredentialsProvider creds = new AWSStaticCredentialsProvider(
				new BasicAWSCredentials("dummy", "dummy"));
		
		AmazonDynamoDB dbClient = AmazonDynamoDBClientBuilder.standard().withCredentials(creds).withEndpointConfiguration(
				new AwsClientBuilder.EndpointConfiguration("http://localhost:8000", "dummy"))
				.build();
				
		dynamoDB = new DynamoDB(dbClient);
				
		try {
				
			List<AttributeDefinition> attributeDefinitions= new ArrayList<AttributeDefinition>();
			attributeDefinitions.add(new AttributeDefinition().withAttributeName("pk").withAttributeType("S"));
	
			List<KeySchemaElement> keySchema = new ArrayList<KeySchemaElement>();
			keySchema.add(new KeySchemaElement().withAttributeName("pk").withKeyType(KeyType.HASH));
	
			CreateTableRequest request = new CreateTableRequest()
			        .withTableName(tableName)
			        .withKeySchema(keySchema)
			        .withAttributeDefinitions(attributeDefinitions)
			        .withProvisionedThroughput(new ProvisionedThroughput()
			            .withReadCapacityUnits(1L)
			            .withWriteCapacityUnits(1L));
	
			Table table = dynamoDB.createTable(request);
	
			try {
				table.waitForActive();
			} catch (InterruptedException e) {
				System.out.println("Erro ao criar tabela");
				e.printStackTrace();
			}

			TableDescription tableDescription =
				    dynamoDB.getTable(tableName).describe();

			System.out.printf("%s: %s \t ReadCapacityUnits: %d \t WriteCapacityUnits: %d",
			    tableDescription.getTableStatus(),
			    tableDescription.getTableName(),
			    tableDescription.getProvisionedThroughput().getReadCapacityUnits(),
			    tableDescription.getProvisionedThroughput().getWriteCapacityUnits());
		}catch(Exception ex) {
		}

		mapper = new DynamoDBMapper(dbClient);
	}
	
	@GetMapping("/getTables")
	public String getList() {
		TableCollection<ListTablesResult> tables = dynamoDB.listTables();
        Iterator<Table> iterator = tables.iterator();

        System.out.println("Listing table names");
        String tabelas = "";
        while (iterator.hasNext()) {
            Table table = iterator.next();
            System.out.println(table.getTableName());
            tabelas += table.getTableName() + ",";
        }
        return tabelas.substring(0, tabelas.length()-1);
	}
	
	@GetMapping("/insert")
	public String insert() {
		try {
	//		Transactional tran = new Transactional("t1", "2021-09-15", 999, new Customer("c2", "sdfs"));
			Transactional tran = new Transactional("t2", "2021-09-18", 999, new Customer("c1", "Valmir"));
			mapper.save(tran);
			System.out.println("Finalizado");
			return "Finalizado Insert com sucesso";
		}catch (Exception e) {
			return "Erro ao inserir o usu�rio, verifique se j� n�o existe";
		}
	}
	
	@GetMapping("/get/{id}")
	public String getInfo(@PathVariable String id) {
		try {
			Transactional tran = mapper.load(Transactional.class, id);
			return tran.toString();
		}catch (Exception e) {
			return "Nenhum resultado da pesquisa";
		}
	}
	
	@GetMapping("/getList/{id}")
	public String getInfoList(@PathVariable String id) {
		try {
			Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
	        eav.put(":val1", new AttributeValue().withS(id));
	//        eav.put(":val2", new AttributeValue().withS("2021-09-15"));
			DynamoDBQueryExpression<Transactional> queryExpression = new DynamoDBQueryExpression<Transactional>()
		            .withKeyConditionExpression("pk = :val1").withExpressionAttributeValues(eav);
	//				.withKeyConditionExpression("pk = :val1 and ReplyDateTime > :val2").withExpressionAttributeValues(eav);
			List<Transactional> tran = mapper.query(Transactional.class, queryExpression);
			return tran.get(0).toString();
		}catch (Exception e) {
			return "Nenhum resultado da pesquisa";
		}
	}
	
	@GetMapping("/update")
	public void update() {
//		Transactional tran = new Transactional("t1", "2021-09-15", 999, new Customer("c2", "sdfs"));
		Transactional tran = new Transactional("t2", "2021-09-18", 998, new Customer("c1", "Valmir1"));
		mapper.batchSave(tran);
		System.out.println("Finalizado Update");
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable String id) {
		try {
	//		Transactional tran = new Transactional("t1", "2021-09-15", 999, new Customer("c2", "sdfs"));
			Transactional tran = new Transactional();
			tran.setPk(id);
			mapper.delete(tran);
			System.out.println("Finalizado Update");
			return "Finalizado Update";
		}catch (Exception e) {
			return "Erro ao excluir o usu�rio, verifique se existe";
		}
	}
	

}
