package br.com.valmirsystems.translator;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.valmirsystems.model.Customer;

public class CustomerTranslator implements DynamoDBTypeConverter<String, Customer> {

	private static final ObjectMapper mapper = new ObjectMapper();
	
	@Override
	public String convert(Customer object) {
		try {
			return mapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Customer unconvert(String object) {
		try {
			return mapper.readValue(object, Customer.class);
		} catch (JsonMappingException e) {
			System.out.println("Erro aqui");
			throw new RuntimeException(e);
		} catch (JsonProcessingException e) {
			System.out.println("Erro aqui1");
			throw new RuntimeException(e);
		}
	}

}
