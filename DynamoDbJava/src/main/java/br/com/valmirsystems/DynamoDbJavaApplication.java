package br.com.valmirsystems;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DynamoDbJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DynamoDbJavaApplication.class, args);
	}

}
