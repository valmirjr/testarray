package br.com.threadtest.valmir;

public class Problems {
    public static Object lock1 = new Object();
    public static Object lock2 = new Object();

    public static void main(String[] args) {
        new Thread1().start();
        new Thread2().start();
    }

    public static class Thread1 extends Thread{
        public void run(){
            synchronized (lock1){
                System.out.println("Thread 1: Has lock");
                try{
                    Thread.sleep(100);
                }catch (InterruptedException e){

                }
                System.out.println("Thread 1: Waiting for lock2");
                synchronized (lock2){
                    System.out.println("Thread 1: Has lock1 waiting lock2");
                }
                System.out.println("Thread 1: Released lock2");
            }
            System.out.println("Thread 1: Released lock1. Exiting...");
        }
    }

    public static class Thread2 extends Thread{
        public void run(){
            synchronized (lock2){
                System.out.println("Thread 2: Has lock");
                try{
                    Thread.sleep(100);
                }catch (InterruptedException e){

                }
                System.out.println("Thread 2: Waiting for lock1");
                synchronized (lock1){
                    System.out.println("Thread 2: Has lock2 waiting lock1");
                }
                System.out.println("Thread 2: Released lock1");
            }
            System.out.println("Thread 2: Released lock2. Exiting...");
        }
    }
}
