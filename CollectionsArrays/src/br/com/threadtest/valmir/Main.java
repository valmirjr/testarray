package br.com.threadtest.valmir;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello from Main");

        Thread anotherThread = new AnotherThread();
        anotherThread.setName("==Another Thread==");
        anotherThread.start();

        new Thread(){
            public void run(){
                System.out.println("Hello from anonymous class thread");
            }
        }.start();

        Thread myRunnable = new Thread(new MyRunnable());
        myRunnable.start();

        Thread myRunnable2 = new Thread(new MyRunnable(){
            @Override
            public void run() {
                System.out.println("Valmir implements anonymous class MyRunnable");
                try{
                    anotherThread.join(2000);
                    System.out.println("AnotherThread terminated, so I`m running again");
                }catch (InterruptedException e){
                    System.out.println("I couldn`t wait after all. I was Interrupted");
                }
            }
        });
        myRunnable2.start();

//        anotherThread.interrupt();

        System.out.println("Hello again from Main thread.");
    }
}
