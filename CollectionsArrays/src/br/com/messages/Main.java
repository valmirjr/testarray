package br.com.messages;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        Message message = new Message();
        (new Thread(new Writer(message))).start();
        (new Thread(new Reader(message))).start();
    }
}

class Message{
    private String message;
    private boolean empty = true;

    public synchronized String read(){
        while (empty){
            try{
                wait();
            }catch (InterruptedException e){

            }
        }
        empty = true;
        notifyAll();
        return message;
    }

    public synchronized void write(String message){
        while (!empty){
            try{
                wait();
            }catch (InterruptedException e){

            }
        }
        empty = false;
        this.message = message;
        notifyAll();
    }
}

class Writer implements Runnable{
    private Message message;

    public Writer(Message message){
        this.message = message;
    }

    public void run(){
        String messages[] = {
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy",
                "text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book",
                "It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged",
                "It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
        };
        Random random = new Random();

        for (int i = 0; i < messages.length; i++){
            message.write(messages[i]);
            try{
                Thread.sleep(random.nextInt(2000));
            } catch (InterruptedException exception) {

            }
        }
        message.write("Finished");
    }
}

class Reader implements Runnable{
    private Message message;

    public Reader(Message message) {
        this.message = message;
    }

    public void run(){
        Random random = new Random();
        for (String lastedMessage = message.read(); !lastedMessage.equals("Finished");
        lastedMessage = message.read()){
            System.out.println(lastedMessage);
            try{
                Thread.sleep(random.nextInt(2000));
            } catch (InterruptedException exception) {

            }
        }
    }
}