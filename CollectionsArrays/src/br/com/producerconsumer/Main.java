package br.com.producerconsumer;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;
import java.util.concurrent.locks.ReentrantLock;

import static br.com.producerconsumer.Main.EOF;

public class Main {
    public static final String EOF = "EOF";

    public static void main(String[] args) {
        ArrayBlockingQueue<String> buffer = new ArrayBlockingQueue<String>(5);
//        List<String> buffer = new ArrayList<>();
        ReentrantLock bufferLock = new ReentrantLock();
        ExecutorService executorService = Executors.newFixedThreadPool(5);

        MyProducer producer = new MyProducer(buffer, ThreadColor.ANSI_YELLOW);
        MyConsumer consumer1 = new MyConsumer(buffer, ThreadColor.ANSI_PURPLE);
        MyConsumer consumer2 = new MyConsumer(buffer, ThreadColor.ANSI_CYAN);

//        MyProducer producer = new MyProducer(buffer, ThreadColor.ANSI_YELLOW, bufferLock);
//        MyConsumer consumer1 = new MyConsumer(buffer, ThreadColor.ANSI_PURPLE, bufferLock);
//        MyConsumer consumer2 = new MyConsumer(buffer, ThreadColor.ANSI_CYAN, bufferLock);

        executorService.execute(producer);
        executorService.execute(consumer1);
        executorService.execute(consumer2);

        Future<String> future = executorService.submit(new Callable<String>() {
            @Override
            public String call() throws Exception {
                System.out.println(ThreadColor.ANSI_WHITE+"I'm being printed for the Callable class");
                return "This is the Callable result";
            }
        });

        try{
            System.out.println(future.get());
        }catch (ExecutionException e){
            System.out.println("Something went wrong");
        }catch (InterruptedException e){
            System.out.println("Thread running the task was interrupted");
        }
        executorService.shutdown();
//        new Thread(producer).start();
//        new Thread(consumer1).start();
//        new Thread(consumer2).start();
    }
}

class MyProducer implements Runnable{
    private ArrayBlockingQueue<String> buffer;
    private String color;
    private ReentrantLock bufferLock;

    public MyProducer(ArrayBlockingQueue<String> buffer, String color) {
        this.buffer = buffer;
        this.color = color;
    }

//    public MyProducer(List<String> buffer, String color, ReentrantLock bufferLock) {
//        this.buffer = buffer;
//        this.color = color;
//        this.bufferLock = bufferLock;
//    }

    public void run(){
        Random random = new Random();
        String[] nums = {"1", "2", "3", "4", "5"};
        for(String num: nums){
            try{
                System.out.println(color + "Adding..." + num);
//                synchronized (buffer) {
//                bufferLock.lock();
                buffer.put(num);
//                buffer.add(num);
//                bufferLock.unlock();
//                }
                Thread.sleep(random.nextInt(1500));
            }catch (InterruptedException e){
                System.out.println("Producer was interrupted");
            }
        }
        System.out.println(color + "Adding EOF and exiting...");
//        synchronized (buffer) {
//        bufferLock.lock();
        try {
            buffer.put("EOF");
        } catch (InterruptedException exception) {

        }
//        buffer.add("EOF");
//        bufferLock.unlock();
//        }
    }
}

class MyConsumer implements Runnable {
    private ArrayBlockingQueue<String> buffer;
    private String color;
    private ReentrantLock bufferLock;

    public MyConsumer(ArrayBlockingQueue<String> buffer, String color) {
        this.buffer = buffer;
        this.color = color;
    }

//    public MyConsumer(List<String> buffer, String color, ReentrantLock bufferLock) {
//        this.buffer = buffer;
//        this.color = color;
//        this.bufferLock = bufferLock;
//    }

    public void run() {
        while(true) {
//            if(bufferLock.tryLock()){
            synchronized (buffer) {
                try {
                    //            synchronized(buffer) {
//                    bufferLock.lock();
                    if (buffer.isEmpty()) {
//                        bufferLock.unlock();
                        continue;
                    }
//                    if(buffer.get(0).equals(EOF)) {
                    if (buffer.peek().equals(EOF)) {
                        System.out.println(color + "Exiting");
//                        bufferLock.unlock();
                        break;
                    } else {
                        System.out.println(color + "Removed " + buffer.take());
//                        System.out.println(color + "Removed " + buffer.remove(0));
                    }
//                    bufferLock.unlock();
//            }
                } catch (InterruptedException exception) {
                    exception.printStackTrace();
                }
            }
//                finally {
//                    bufferLock.unlock();
//                }
//            }
        }
    }
}