package br.com.arrayslist;

import java.util.HashMap;
import java.util.Map;

public class Maps {
    private Map<Integer, String> items = new HashMap<>();
    private static int counter = 0;

    public Maps() {
    }

    public Maps(Map<Integer, String> items) {
        this.items = items;
    }

    public void addItem(String item){
        counter++;
        items.put(counter, item);
    }

    public void remove(int key){
        items.remove(key);
    }

    public boolean find(int key){
        return items.containsKey(key);
    }

    public boolean find(String item){
        return items.containsValue(item);
    }

    public void showKeys(){
        System.out.println("Your Map have " + items.size() + " elements");
        for (int key : items.keySet()){
            System.out.println("Item " + key + " : " + items.get(key));
        }
    }
}
