package br.com.arrayslist;

import java.util.ArrayList;

public class Arraylist {
    private ArrayList<String> grocery = new ArrayList<>();

    public void addItemGrocery(String item){
        grocery.add(item);
    }

    public void insertItemGrocery(int position, String item){
        grocery.add(position, item);
    }

    public void printArraylist(){
        System.out.println("Your arrayList have " + grocery.size() + " elements");
        for(int i = 0; i < grocery.size(); i++){
            System.out.println("Item " + (i+1) + ": " + grocery.get(i));
        }
    }

    public void modifyArrayList(int position, String newItem){
        System.out.println(findItem(position));
        if(findItem(position) && (findItem(newItem) == -1)) {
            grocery.set(position, newItem);
            System.out.println("ArrayList " + position + " has been modified");
        }else{
            System.out.println("Position out of range");
        }
    }

    public void removeArrayList(int position){
        if(findItem(position)) {
            String theItem = grocery.get(position-1);
            grocery.remove(position);
        }
    }

    public int findItem(String searchItem){
//        boolean exists = grocery.contains(searchItem);
        return grocery.indexOf(searchItem);
    }

    public boolean findItem(int searchItem){
        return grocery.size() >= searchItem;
    }
}
