package br.com.arrayslist;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

public class Linkedlist {
    private LinkedList<String> placesToVisit = new LinkedList<>();

    public void addItem(String item){
        placesToVisit.add(item);
    }

    public void insertItem(int position, String item){
        placesToVisit.add(position, item);
    }

    public boolean insertItemInOrder(String item){
        ListIterator<String> i = placesToVisit.listIterator();
        while(i.hasNext()){
            int comparison = i.next().compareTo(item);
            if(comparison == 0){
                System.out.println(item + " is already included on linkedList");
                return false;
            }else if(comparison > 0){
                i.previous();
                i.add(item);
                return true;
            }else if(comparison < 0){

            }
        }
        i.add(item);
        return true;
    }

    public void modifyItem(int position, String newItem){
        System.out.println(findItem(position));
        if(findItem(position) && (findItem(newItem) == -1)) {
            placesToVisit.set(position, newItem);
            System.out.println("ArrayList " + position + " has been modified");
        }else{
            System.out.println("Position out of range");
        }
    }

    public void removeItem(int position){
        if(findItem(position)) {
            String theItem = placesToVisit.get(position-1);
            placesToVisit.remove(position);
        }
    }

    public int findItem(String searchItem){
//        boolean exists = grocery.contains(searchItem);
        return placesToVisit.indexOf(searchItem);
    }

    public boolean findItem(int searchItem){
        return placesToVisit.size() >= searchItem;
    }

    public int search(String item){
        long start = System.currentTimeMillis();
        int result = Collections.binarySearch(placesToVisit, item, null);
        System.out.println("Delay Collection = " + (System.currentTimeMillis() - start));
        long startC = System.currentTimeMillis();
        int rest = placesToVisit.indexOf(item);
        System.out.println("Delay indexof = " + (System.currentTimeMillis() - startC));
        return result;
    }

    public void printLinkedlist(){
        Iterator i = placesToVisit.iterator();
        int count = 0;
        System.out.println("Your LinkedList have " + placesToVisit.size() + " elements");
        while (i.hasNext()){
            count++;
            System.out.println("Item " + (count) + ": " + i.next());
        }

    }

}
