package br.com.arrayslist;

import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);
    private static Arraylist arraylist = new Arraylist();
    private static Linkedlist linkedList = new Linkedlist();
    private static Maps maps = new Maps();
    //Collection set is on order

    public static void main(String[] args) {
	    boolean quit = false;
	    int choose = 0;
        printInstructions();
	    while (!quit){
            System.out.println("Enter your choice:");
            choose = scanner.nextInt();
            scanner.nextLine();
            switch (choose){
                case 0:
                    printInstructions();
                    break;
                case 1:
                    addItem();
                    break;
                case 2:
                    modifyItem();
                    break;
                case 3:
                    removeItem();
                    break;
                case 4:
                    searchItem();
                    break;
                case 5:
                    arraylist.printArraylist();
                    linkedList.printLinkedlist();
                    maps.showKeys();
                    break;
                case 6:
                    quit = true;
                    break;
            }
        }
    }

    public static void printInstructions(){
        System.out.println("\nPress");
        System.out.println("\t0 - Menu");
        System.out.println("\t1 - Add item");
        System.out.println("\t2 - Modify item");
        System.out.println("\t3 - Remove item");
        System.out.println("\t4 - Search item");
        System.out.println("\t5 - List item");
        System.out.println("\t6 - Quit");
    }

    public static void addItem(){
        System.out.println("Type your item to be add");
        String item = scanner.nextLine();

        if(!item.trim().isEmpty()){
            arraylist.addItemGrocery(item);
            linkedList.addItem(item);
            maps.addItem(item);
        }else{
            System.out.println("Can not be empty");
        }
    }

    public static void modifyItem(){
        System.out.println("Type your position item to be modify");
        int position = scanner.nextInt();
        scanner.nextLine();
        System.out.println("Type your new item to make modify");
        String item = scanner.nextLine();

        if(!item.trim().isEmpty()){
            arraylist.modifyArrayList(position-1, item);
            linkedList.modifyItem(position-1, item);
        }else{
            System.out.println("Can not be empty");
        }
    }

    public static void removeItem(){
        System.out.println("Type your item to be delete");
        int item = scanner.nextInt();
        scanner.nextLine();

        arraylist.removeArrayList(item);
        linkedList.removeItem(item);
    }

    public static void searchItem(){
        System.out.println("Type your item to be search");
        String item = scanner.nextLine();

        if(arraylist.findItem(item) >= 0){
            System.out.println("Item: " + arraylist.findItem(item));
            linkedList.search(item);
        }else{
            System.out.println("Item not found");
        }
    }
}
